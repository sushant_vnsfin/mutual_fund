import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginflashscreenComponent } from './onboarding/loginflashscreen.component';
import { CongratulationsComponent } from './onboarding/congratulations.component';
import { VerificationComponent } from './onboarding/verification.component';
import { MobileVerificationComponent } from './onboarding/mobile-verification.component';
import { OtpScreenComponent } from './onboarding/otp-screen.component';
import { PanNumberComponent } from './onboarding/pan-number.component';
import { MutualFundModalComponent } from './mutual-fund-modal/mutual-fund-modal.component';
import { PersonalDetailsComponent } from './onboarding/personal-details.component';
import { BankDetailsComponent } from './onboarding/bank-details.component';
import { KycVerificationComponent } from './onboarding/kyc-verification.component';
import { IfcCodeComponent } from './onboarding/ifc-code.component';
import { SignaturepadVerificationComponent } from './onboarding/signaturepad-verification.component';
import { PhotoComponent } from './onboarding/photo.component';
import { LoginComponent } from './login/login.component';
import { ResetpasswordComponent } from './login/resetpassword.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { SuccessComponent } from './account-details/success.component';
import { MandateComponent } from './account-details/mandate.component';
import { ChangepasswordComponent } from './account-details/changepassword.component';
import { EmailComponent } from './onboarding/email.component';
import { SetPasswordComponent } from './onboarding/set-password.component';
import { SettingsComponent } from './account-details/settings.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HelpComponent } from './account-details/help.component';
import { ViewdetailsComponent } from './account-details/viewdetails.component';
import { ShareandearnComponent } from './account-details/shareandearn.component';
import { ReportComponent } from './account-details/report.component';
import { BankmandatedetailsComponent } from './account-details/bankmandatedetails.component';
import { OnetimemandateComponent } from './buying-flow/onetimemandate.component';
import { OtmuploadComponent } from './buying-flow/otmupload.component';
import { FundDetailsComponent } from './buying-flow/fund-details.component';
import { OrderhistoryComponent } from './order-history/orderhistory.component';
import { SipDateComponent } from './buying-flow/sip-date.component';
import { OrderhistorylumpsumComponent } from './order-history/orderhistorylumpsum.component';
import { PurchasesipComponent } from './buying-flow/purchasesip.component';
import { SipPaymentComponent } from './buying-flow/sip-payment.component';
import { SipSuccessComponent } from './buying-flow/sip-success.component';
import { SipBankSelectComponent } from './buying-flow/sip-bank-select.component';
import { OrderHistorySipComponent } from './order-history/order-history-sip.component';
import { SellScreenComponent } from './redemption/sell-screen.component';
import { EditSipComponent } from './order-history/edit-sip.component';
import { PortfolioInvestmentComponent } from './fund-portfolio/portfolio-investment.component';
import { FundTransactionComponent } from './fund-portfolio/fund-transaction.component';
import { FundfilterComponent } from './fund-filter/fundfilter.component';
import { AuthGuard} from './helpers/auth.guard';
import { loginAuthGuard} from './helpers/loginAuth.guard';
import { CheckUploadComponent } from './onboarding/check-upload.component';
import { Error404Component } from './error/error404.component';
import { Error500Component } from './error/error500.component';
import { FatcaDetailsComponent } from './onboarding/fatca-details.component';

const routes: Routes = [
  { path: '', component: LoginflashscreenComponent},
  { path: 'congratulations', component: CongratulationsComponent,  canActivate: [AuthGuard]},
  { path: 'verification', component: VerificationComponent,  canActivate: [AuthGuard]},
  { path: 'mobile-verification', component: MobileVerificationComponent,  canActivate: [AuthGuard]},
  { path: 'otp', component: OtpScreenComponent,  canActivate: [AuthGuard]},
  { path: 'pan-number', component: PanNumberComponent,  canActivate: [AuthGuard]},
  { path: 'mutualfund-modal', component: MutualFundModalComponent,  canActivate: [AuthGuard]},
  { path: 'personal-details', component: PersonalDetailsComponent,  canActivate: [AuthGuard]},
  { path: 'fatca-details', component: FatcaDetailsComponent,  canActivate: [AuthGuard]},
  { path: 'bank-details', component: BankDetailsComponent,  canActivate: [AuthGuard]},
  { path: 'kyc-verification', component: KycVerificationComponent,  canActivate: [AuthGuard]},
  { path: 'ifsc-code', component: IfcCodeComponent,  canActivate: [AuthGuard]},
  { path: 'signaturepad-verification', component: SignaturepadVerificationComponent,  canActivate: [AuthGuard]},
  { path: 'photo', component: PhotoComponent,  canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent},
  { path: 'reset-password', component: ResetpasswordComponent},
  { path: 'account-details', component: AccountDetailsComponent,  canActivate: [AuthGuard]},
  { path: 'success', component: SuccessComponent,  canActivate: [AuthGuard]},
  { path: 'show-bank', component: MandateComponent,  canActivate: [AuthGuard]},
  { path: 'change-password', component: ChangepasswordComponent},
  { path: 'email', component: EmailComponent},
  { path: 'set-password', component: SetPasswordComponent},
  { path: 'settings', component: SettingsComponent,  canActivate: [AuthGuard]},
  { path: 'home', component: HomePageComponent, canActivate: [AuthGuard]},
  { path: 'help', component: HelpComponent,  canActivate: [AuthGuard]},
  { path: 'profile', component: ViewdetailsComponent,  canActivate: [AuthGuard]},
  { path: 'share-and-earn', component: ShareandearnComponent,  canActivate: [AuthGuard]},
  { path: 'reports', component: ReportComponent,  canActivate: [AuthGuard]},
  { path: 'view-bank-details', component: BankmandatedetailsComponent,  canActivate: [AuthGuard]},
  { path: 'one-time-mandate', component: OnetimemandateComponent,  canActivate: [AuthGuard]},
  { path: 'otm-upload', component: OtmuploadComponent,  canActivate: [AuthGuard]},
  { path: 'fund-details', component: FundDetailsComponent,  canActivate: [AuthGuard]},
  { path: 'order-history', component: OrderhistoryComponent,  canActivate: [AuthGuard]},
  { path: 'order-details', component: OrderhistorylumpsumComponent,  canActivate: [AuthGuard]},
  { path: 'sip-date', component: SipDateComponent,  canActivate: [AuthGuard]},
  { path: 'purchase-sip', component: PurchasesipComponent,  canActivate: [AuthGuard]},
  { path: 'sip-payment', component: SipPaymentComponent,  canActivate: [AuthGuard]},
  { path: 'sip-success', component: SipSuccessComponent,  canActivate: [AuthGuard]},
  { path: 'sip-bank-select', component: SipBankSelectComponent,  canActivate: [AuthGuard]},
  { path: 'order-history-sip', component: OrderHistorySipComponent,  canActivate: [AuthGuard]},
  { path: 'edit-sip', component: EditSipComponent,  canActivate: [AuthGuard]},
  { path: 'portfolio', component: PortfolioInvestmentComponent,  canActivate: [AuthGuard]},
  { path: 'fund-transaction', component: FundTransactionComponent,  canActivate: [AuthGuard]},
  { path: 'sell-fund', component: SellScreenComponent,  canActivate: [AuthGuard]},
  { path: 'fund-filter', component: FundfilterComponent},
  { path: 'cheque-upload', component: CheckUploadComponent },
  { path: '404-page', component: Error404Component},
  { path: '500-page', component: Error500Component},
  { path: '', redirectTo: 'help', pathMatch: 'full'},
  { path: '**', redirectTo: 'help', pathMatch: 'full'},
];

@NgModule({
  // imports: [RouterModule.forRoot(routes, { initialNavigation: false })],
 // imports: [RouterModule.forRoot(routes)],
 imports: [RouterModule.forRoot(routes, {
  scrollPositionRestoration: 'enabled', // Add options right here
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
