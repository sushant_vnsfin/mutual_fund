import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-orderhistorylumpsum',
  templateUrl: './orderhistorylumpsum.component.html',
  styleUrls: ['./orderhistorylumpsum.component.css']
})

export class OrderhistorylumpsumComponent implements OnInit {
  modalReference: NgbModalRef;
  order: any;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;

  constructor(public location: PlatformLocation, private modalService: NgbModal, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    let key = 'orderDetails';
    this.order = JSON.parse(localStorage.getItem(key));
  }

  // Function to open modal to view rejection reason
  openOrderHistory(orderHistory: any) {
    this.modalReference = this.modalService.open(orderHistory, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop' });
  }

  // Function to restart buying if order failed/cancelled
  restartBuying(order): void {
    setTimeout(() => {
      let key = 'scheme_code';
      var object = {};
      this.loading = true;
      object['scheme_code'] = this.order.scheme_code;
      object['isin'] = this.order.isin;
      this.apiService.getFundDetails(object).subscribe(
        res => {
          this.loading = false;
          let key = 'scheme_details';
          localStorage.setItem(key, JSON.stringify(res.data));
          this.router.navigate(['/fund-details']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )

    }, 200);

  }
}
