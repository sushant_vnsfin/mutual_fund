import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
enum CheckBoxType { High_to_Low, Low_to_High, NONE };

@Component({
  selector: 'app-fundfilter',
  templateUrl: './fundfilter.component.html',
  styleUrls: ['./fundfilter.component.css']
})

export class FundfilterComponent implements OnInit {
  showBankSearch: boolean;
  public isViewable: boolean;
  selectedItem: any;
  navdate: any;
  loading = false;
  orderHistory: [];
  fund: any;
  funds: any;
  risk: any;
  asset_type: any;
  category: any;
  errorStatus: any;
  investment_plan: any;
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  constructor(public location: Location, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { }

  ngOnInit() {
    this.getFilteredFunds('1M');

    /*get investment plan info*/
    let invest_plan = 'investment_plan';
    this.investment_plan = localStorage.getItem(invest_plan);
    /*get investment plan info*/

    /*get risk info*/
    let riskkey = 'risk';
    this.risk = localStorage.getItem(riskkey);
    /*get risk info*/

    /*get asset_type info*/
    let asset = 'asset_type';
    this.asset_type = localStorage.getItem(asset);
    /*get asset_type info*/

    /*get category info*/
    let category_key = 'category';
    this.category = localStorage.getItem(category_key);
    /*get category info*/

    // To get scheme data
    let key = 'riskFundDetails';
    this.fund = JSON.parse(localStorage.getItem(key));
  }

  public monthPeriod: any = [
    { period: '1M', name: '1 M' },
    { period: '3M', name: '3 M' },
    { period: '1Y', name: '1 Y' },
    { period: '3Y', name: '3 Y' },
    { period: '5Y', name: '5 Y' },
    { period: ' ', name: 'max' },
  ];

  /*Start Nav Chart Data*/
  getFilteredFunds(navdate): void {
    this.selectedItem = navdate;
  }

  // Filter section
  equity = [{ type: "Small Cap", }, { type: "Mid-Cap", }, { type: "Large Cap", }, { type: "ELSS", }, { type: "Any", }, { type: "Sectorial", }, { type: "Theamatic", }, { type: "Contra", }, { type: "Dividend Yield", }, { type: "Focused", }, { type: "Value", }, { type: "Large and Mid-Cap", },]
  debt = [{ type: "Long Duration", }, { type: "Medium Duration", }, { type: "Short Duration", }, { type: "Ultra ShortDuration", }, { type: "Gilt", }, { type: "Gilt10 Year", }, { type: "Liquid", }, { type: "Any", }, { type: "Money Market", }, { type: "Overnight", }, { type: "Floater", }, { type: "Medium to Long Duration", }, { type: "Banking and PSU", }, { type: "Corporate Bond", }, { type: "Credit Risk", }, { type: "Dynamic", },]
  hybrid = [{ type: "Aggressive", }, { type: "Arbitrage", }, { type: "Balanced Advantage", }, { type: "Conservative", }, { type: "Multi Asset Allocation", }, { type: "Equity Saving", }, { type: "Any", }]
  soln = [{ type: "Retirement", }, { type: "Child", }, { type: "Any", }]

  selectedUserTab = 1;
  tabs = [
    { name: 'Equity', key: 1, active: true },
    { name: 'Debt', key: 2, active: false },
    { name: 'Hybrid', key: 3, active: false },
    { name: 'Schemes', key: 4, active: false },
    { name: 'Solution Oriented', key: 5, active: false },
  ];

  tabChange(selectedTab) {
    this.selectedUserTab = selectedTab.key;
    for (let tab of this.tabs) {
      if (tab.key === selectedTab.key) {
        tab.active = true;
      }
      else {
        tab.active = false;
      }
    }
  }

  // Function to select only one checkbox at once 
  check_box_type = CheckBoxType;
  currentlyChecked: CheckBoxType;

  selectCheckBox(targetType: CheckBoxType) {
    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.currentlyChecked === targetType) {
      this.currentlyChecked = CheckBoxType.NONE;
      return;
    }
  }

  cancelBtn(event: Event) {
    this.showBankSearch = !this.showBankSearch;
    event.stopPropagation();
  }

  showOrHideBankName() {
    this.showBankSearch = !this.showBankSearch;
  }

  uncheckAll() {
    this.checkboxes.forEach((element) => {
      element.nativeElement.checked = false;
    });
  }
  // Filter section

  // Function to get scheme details
  investInScheme(funds): void {
    this.loading = true;
    let key = 'scheme_code';
    var object = {};
    object['isin'] = funds.isin;
    object['acc_fin_scheme_code'] = funds.accord_scheme_code;
    object['scheme_code'] = funds.bse_scheme_code;

    this.apiService.getFundDetails(object).subscribe(
      res => {
        this.loading = false;
        let key = 'scheme_details';
        localStorage.setItem(key, JSON.stringify(res.data));
        let isinkey = 'scheme_isin';
        localStorage.setItem(isinkey, JSON.stringify(funds.isin));
        this.router.navigate(['/fund-details']);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

}
