import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-loginflashscreen',
  templateUrl: './loginflashscreen.component.html',
  styleUrls: ['./loginflashscreen.component.css']
})
export class LoginflashscreenComponent implements OnInit {

  carouselOptions = {
      margin: 0,
      nav: true,
      loop: true,
      autoplay:true,
      autoplayTimeout:2000,
      navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
      responsiveClass: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        },
        1500: {
          items: 1
        }
      }
    }

    images = [
      {
        text: "100% Paperless SIP",
        image: "../assets/images/100__paperless_SIP.svg"
      },
      {
        text: "Investing made simple",
        image: "../assets/images/Investing_made_simple.svg"
      },
      {
        text: "Filters & Details of fund",
        image: "../assets/images/filter-details-fund.svg"
      }

    ]
  loginflag: any;
  
  constructor(private router: Router) { }

  auth: string;
  ngOnInit() {
    let key = 'authentication_token';
    this.auth = localStorage.getItem(key);
    if(this.auth != null){
      this.router.navigate(['/home']);
      this.loginflag = true;
      let key = 'loginflag';
      localStorage.setItem(key, JSON.stringify(this.loginflag));
    }else{
      this.loginflag = false;
      let key = 'loginflag';
      localStorage.setItem(key, JSON.stringify(this.loginflag));
    }
  }

  loginSign(object){
    if(object == 'login'){
        this.router.navigate(['/login']);
    }
    else{
        this.router.navigate(['/email']);
    }

  }
}
