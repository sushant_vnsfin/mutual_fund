import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { IFSCbankSelect } from '../helpers/must-match.validator';
// import {NgbTypeahead, NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, catchError, switchMap } from 'rxjs/operators';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-ifc-code',
  templateUrl: './ifc-code.component.html',
  styleUrls: ['./ifc-code.component.css']
})

export class IfcCodeComponent implements OnInit {
  public model: any;
  public model1: any;
  loading = false;
  errorMessage: any;
  errorStatus: any;
  conShow = false;
  otherError = false;
  showSearch = true;
  resbankName: any;
  resbankAdd: any;
  resbankBranch: any;
  ifscData: any;
  public states1: any;
  public branchName: any;
  ifscobject = {};
  noResult: boolean;
  noResult_branch: boolean;
  selectedValue: string;
  selectedValue1: string;




  bankChanged(event: TypeaheadMatch): void {

    setTimeout(() => {
      this.ifscobject['bo_ifsc'] = event.item.BO_ifsc;
      this.loading = true;
      this.apiService.getBranchlist(this.ifscobject).subscribe(
        res => {
          this.loading = false;
          this.branchName = res.data;
          this.otherError = false;
        }
      )
    }, 200);

  }

  typeaheadNoResults(event: boolean): void {
    this.noResult = event;
  }

  typeaheadNoResults1(event: boolean): void {
    this.noResult_branch = event;
  }


  /*branch changed*/
  branchChanged(event: TypeaheadMatch) {
    setTimeout(() => {
      this.loading = true;
      this.showBankSearch = !this.showBankSearch;
      this.serachbankElementRef.nativeElement.classList.remove("show-searchbank");
      var object = {};
      object['ifsc'] = event.item.IFSC;
      this.ifsccodeForm.setValue({ ifsc: event.item.IFSC });
      this.apiService.getBankifsc(object).subscribe(
        res => {
          this.loading = false;
          this.conShow = true;
          this.resbankName = res.data.bank_details.BANK;
          this.resbankAdd = res.data.bank_details.ADDRESS;
          this.resbankBranch = res.data.bank_details.BRANCH;
          let ifscData = res;
          let key = 'ifsc';
          localStorage.setItem(key, JSON.stringify(ifscData));

        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.ifsccodeForm);
          this.errorStatus = error.status;
          if (this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
      this.ifsearchForm.get('branchname').reset();
      this.ifsearchForm.get('bankName').reset();
    }, 1000);

  }
  /*branch changed*/



  /*Start search bank name*/
  /*show and hide ifsc bank serach*/
  showOrHideBankName() {
    this.showBankSearch = !this.showBankSearch;
    if (this.showBankSearch) {
      this.serachbankElementRef.nativeElement.classList.add("show-searchbank");
      this.serachbanknameElementRef.nativeElement.focus();
      this.apiService.getBanklist().subscribe(
        res => {
          this.states1 = res.data;
        },
        (error) => { }
      )
    }
    else {
      this.serachbankElementRef.nativeElement.classList.remove("show-searchbank");
      this.ifsearchForm.get('branchname').reset();
      this.ifsearchForm.get('bankName').reset();

    }

  }




  // searchBank = (text$: Observable<string>) => {
  //   const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
  //   const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
  //   const inputFocus$ = this.focus$.pipe(filter(() => !this.instance.isPopupOpen()));

  //   return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
  //     map(term => (term === '' ? this.states1
  //       : this.states1.filter(v => v.Bank_Name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
  //       );
  // }
  /*Start end bank name*/

  /*Start search branch name*/
  // searchBranch = (text$: Observable<string>) => {
  //   const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
  //   const clicksWithClosedPopup$ = this.clickb$.pipe(filter(() => !this.instance.isPopupOpen()));
  //   const inputFocus$ = this.focusb$;

  //   return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
  //     map(term => (term.length < 2 ? []
  //       : this.branchName.filter(v => v.BRANCH.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
  //   );
  // }
  /*End search branch name*/

  ifsccodeForm: FormGroup;
  ifsearchForm: FormGroup;
  submitted = false;
  showBankSearch = false;
  showBankList = false;


  @ViewChild('ifscnumberRef') ifscnumberElementRef: ElementRef;
  @ViewChild('serachbankRef') serachbankElementRef: ElementRef;
  @ViewChild('serachbanknameRef') serachbanknameElementRef: ElementRef;



  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: Location) { }

  ngAfterViewInit() {
    this.ifscnumberElementRef.nativeElement.focus();
  }




  ngOnInit() {

    this.ifsccodeForm = this.fb.group({
      ifsc: ['', [Validators.required, Validators.pattern('^([a-zA-Z0-9]){11}?$')]]
    });

    this.ifsearchForm = this.fb.group({
      branchname: [''],
      bankName: [''],
      bankCode: [''],
    });
  }


  // convenience getter for easy access to form fields
  get f() { return this.ifsccodeForm.controls; }


  /*Show Bank List Function*/
  showHidefun() {
    this.showBankSearch = !this.showBankSearch;
  }

  /*Confirm Ifsc code */
  confirmIfsc() {
    this.submitted = true;
    if (this.ifsccodeForm.valid) {
      this.loading = true;
      const ifscFormdata = JSON.stringify(this.ifsccodeForm.controls['ifsc'].value);
      var object = {};
      object['ifsc'] = JSON.parse(ifscFormdata);
      this.apiService.getBankifsc(object).subscribe(
        res => {
          this.loading = false;
          this.conShow = true;
          this.otherError = false;
          this.resbankName = res.data.bank_details.BANK;
          this.resbankAdd = res.data.bank_details.ADDRESS;
          this.resbankBranch = res.data.bank_details.BRANCH;
          let ifscData = res;
          let key = 'ifsc';
          localStorage.setItem(key, JSON.stringify(ifscData));

        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.ifsccodeForm);
          this.errorStatus = error.status;
          if (this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )

    }
  }
  /*Confirm Ifsc code*/


  /*final Save IFSC*/
  saveIFSC(): void {
    this.submitted = true;
    if (this.ifsccodeForm.valid) {
      this.router.navigate(['/bank-details']);
    }
  }
  /*final Save IFSC*/


  /*ifsc changed code*/
  ifscChanged(): void {
    this.submitted = true;
    if (this.ifsccodeForm.valid && this.conShow) {
      this.loading = true;
      const ifscFormdata = JSON.stringify(this.ifsccodeForm.controls['ifsc'].value);
      var object = {};
      object['ifsc'] = JSON.parse(ifscFormdata);
      this.apiService.getBankifsc(object).subscribe(
        res => {
          this.loading = false;
          this.conShow = true;
          this.otherError = false;
          this.resbankName = res.data.bank_details.BANK;
          this.resbankAdd = res.data.bank_details.ADDRESS;
          this.resbankBranch = res.data.bank_details.BRANCH;
          let ifscData = res;
          let key = 'ifsc';
          localStorage.setItem(key, JSON.stringify(ifscData));

        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.ifsccodeForm);
          this.errorStatus = error.status;
          if (this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  }
  /*ifsc changed code*/

}
