import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-check-upload',
  templateUrl: './check-upload.component.html',
  styleUrls: ['./check-upload.component.css']
})
export class CheckUploadComponent implements OnInit {
  closeResult: string;
  modalReference: NgbModalRef;
  bankdetails: any;
  mandate_flag: any;
  public showGallary = false;
  loading: boolean;
  resend = false;
  errorStatus: any;

  constructor(private modalService: NgbModal, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    let key = 'account_details';
    this.bankdetails = JSON.parse(localStorage.getItem(key));
    let mandate_key = 'mandate_flag';
    this.mandate_flag = JSON.parse(localStorage.getItem(mandate_key));
  }

  /* Functions to open different modals */
  openVerticallyCentered(content) {
    this.modalReference = this.modalService.open(content, { centered: true, windowClass: 'in otm-upload-custom-class', backdropClass: 'change-modal-backdrop' });
  }

  openUpload(upload) {
    this.modalReference = this.modalService.open(upload, { windowClass: 'in select-otm-upload', backdropClass: 'change-modal-backdrop' });
  }

  openMandateUpload(mandateUpload) {
    this.modalReference = this.modalService.open(mandateUpload, { windowClass: 'in submit-otm-upload', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }
  /* Functions to open different modals */


  /*Image Upload*/
  fileToUpload: File = null;
  imageUrl: string = "";
  /*file upload*/
  processFile(file: FileList) {
    this.fileToUpload = file.item(0);
    //show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.showGallary = true;
    this.modalReference.close();
  }

  /*Submit Mandate*/
  submitMandate(): void {
    this.loading = true;
    var object = {};
    object['mandate_details_id'] = this.bankdetails.mandate_details_id;
    object['singed_mandate'] = this.imageUrl;
    this.apiService.uploadMandate(object).subscribe(
      res => {
        this.loading = false;
      },
      (error) => {
       this.loading = false;
       this.errorHaldlingService.setGlobalValidators(error);
      }
    );
  }
  /*Submit Mandate*/

  nextButton(): void {
    this.modalReference.close();
    this.router.navigate(['/show-bank']);
  }

}
