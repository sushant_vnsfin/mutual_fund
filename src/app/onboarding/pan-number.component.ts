import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { Directive } from '@angular/core';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pan-number',
  templateUrl: './pan-number.component.html',
  styleUrls: ['./pan-number.component.css']
})
export class PanNumberComponent implements AfterViewInit, OnInit {
  pannumberForm: FormGroup;
  submitted = false;
  loading = false;
  errorMessage = '';
  errorStatus = '';
  otherError = false;
  conShow = false;
  resPanNo: any;
  authToken: string;
  status: string;
  modalReference: NgbModalRef;

  @ViewChild('pannumberRef') pannumberElementRef: ElementRef;
  constructor(public location: PlatformLocation, private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private modalService: NgbModal) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
   }

  ngAfterViewInit() {
    this.pannumberElementRef.nativeElement.focus();
  }

  ngOnInit() {
    /*check auth token and go to the home page*/
    let auth_key = 'authentication_token';
    this.authToken = localStorage.getItem(auth_key)
    let account_status = 'account_status';
    this.status = localStorage.getItem(account_status);
    if (this.authToken !== null && this.status >= '30') {
      this.loading = true;
      this.router.navigate(['/home']);
    }
    /*check auth token and go to the home page*/
    this.pannumberForm = this.fb.group({
      pan: ['', [Validators.required, Validators.pattern('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$')]],
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.pannumberForm.controls; }

    // Function to CVLKRA modal
    cvlKraPopup(cvlKra) {
      this.modalReference = this.modalService.open(cvlKra, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
    }


  // Function to show pan number onblur
  checkPanNo() {
    if (this.f.pan.valid) {
      this.submitted = true;
      this.otherError = false
      this.loading = true;
      const panFormdata = JSON.stringify(this.pannumberForm.value);
      const userPan = JSON.parse(panFormdata);

      this.apiService.showPanName(userPan).subscribe(
        res => {
          this.loading = false;
          this.conShow = true;
          this.resPanNo = res.data.name;
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.pannumberForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  }

  savePannumber(): void {
    this.submitted = true;
    if (this.pannumberForm.valid) {
      this.loading = true;
      const panFormdata = JSON.stringify(this.pannumberForm.value);
      const userPan = JSON.parse(panFormdata);
      this.apiService.insertPan(userPan).subscribe(
        res => {
          this.loading = false;
          let account_status = 'account_status';
          localStorage.setItem(account_status, '30');
          this.router.navigate(['/personal-details']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.pannumberForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400" && this.errorStatus != "405") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }

          if(this.errorStatus == "405"){
            let element: HTMLElement = document.getElementById('cvlKraBtn') as HTMLElement;
            element.click();
          }


        }
      )


    }
  }

}
