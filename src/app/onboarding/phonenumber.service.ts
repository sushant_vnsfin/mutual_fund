import { Injectable } from '@angular/core';
import { Phonenumber } from '../models/mobile-number.model';
import { MobileVerificationComponent } from '../onboarding/mobile-verification.component';

@Injectable()
export class PhonenumberService {
    private otpPhonenumber: Phonenumber[] = [
        {
          mobile: null
        },
    ];

    getPhonenumber(): Phonenumber[] {
        return this.otpPhonenumber;
    }

    save(phonenumber: Phonenumber) {
    this.otpPhonenumber.push(phonenumber);
}
}
