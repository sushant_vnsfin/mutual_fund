import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../helpers/must-match.validator';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
  bankDetailsForm: FormGroup;
  showBank = true;
  submitted = false;
  loading = false;
  errorMessage = '';
  ocrErrormsg = '';
  loginkey = 'loginflag';
  bankkey = 'bankflag';
  errorStatus = '';
  @ViewChild('bankdetailsRef') bankdetailsElementRef: ElementRef;
  @ViewChild('showhideRef') showhideElementRef: ElementRef;
  authToken: string;
  status: string;
  loginflag: any;
  bankflag: any;
  otherError: any;
  ocrError: any;
  modalReference: NgbModalRef;
  public showGallary = false;
  bankdetails: any;
  sipPaymentFlag: any;
  banklist: any;
  bankMandate: any;
  rowsControls = [];

  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private modalService: NgbModal, public location: Location, private routerService: RouterExtService) { 

    this.bankflag = JSON.parse(localStorage.getItem(this.bankkey));
    const prev_url = this.routerService.getPreviousUrl();
    let auth_key = 'authentication_token';
    this.authToken = localStorage.getItem(auth_key)
    let account_status = 'account_status';
    this.status = localStorage.getItem(account_status);
    this.loginflag = JSON.parse(localStorage.getItem(this.loginkey));
    console.log(prev_url, this.status, this.loginflag, this.authToken);

    this.sipPaymentFlag = JSON.parse(localStorage.getItem('sip_pay_addBank'));

                  if(this.authToken !== null && this.status >= '50' && (this.loginflag != true || this.loginflag == null)){
                    this.loading = true;
                    this.router.navigate(['/home']);
                  }
  }

  ngAfterViewInit(){
      this.bankdetailsElementRef.nativeElement.focus();
  }

  ngOnInit() {
                  /*check auth token and go to the home page
                  let auth_key = 'authentication_token';
                  this.authToken = localStorage.getItem(auth_key)
                  let account_status = 'account_status';
                  this.status = localStorage.getItem(account_status);
                  if(this.authToken !== null && this.status >= '50' && this.loginflag === false){
                    this.loading = true;
                    this.router.navigate(['/home']);
                  }
                  check auth token and go to the home page*/


    this.bankDetailsForm = this.fb.group({
      confBanknumber: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(18)]],
      account_number: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(18)]],
      type: ['', [Validators.required]]
    }, {
            validator: MustMatch('account_number', 'confBanknumber')
        });
  }

  cancelCheque(content) {
    this.modalReference = this.modalService.open(content, { centered: true, windowClass: 'in otm-upload-custom-class', backdropClass: 'change-modal-backdrop' });
  }

  openUpload(upload) {
    this.modalReference = this.modalService.open(upload, { windowClass: 'in select-otm-upload', backdropClass: 'change-modal-backdrop' });
  }

  // convenience getter for easy access to form fields
  get f() { return this.bankDetailsForm.controls; }

  /*Image Upload*/
  fileToUpload: File = null;
  imageUrl: string = "";
  /*file upload*/
  processFile(file: FileList) {
    this.fileToUpload = file.item(0);
    //show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
      
    }
    reader.readAsDataURL(this.fileToUpload);
    this.showGallary = true;
    this.modalReference.close();
  }

  /*Submit Mandate*/
  submitMandate(): void {
    this.loading = true;
    var object = {};
    object['mandate_details_id'] = this.bankdetails.mandate_details_id;
    object['singed_mandate'] = this.imageUrl.replace("data:image/jpeg;base64,", "");
    this.apiService.uploadMandate(object).subscribe(
      res => {
        this.loading = false;
      },
      (error) => {
       this.loading = false;
       this.errorHaldlingService.setGlobalValidators(error);
      }
    );
  }
  /*Submit Mandate*/

showOrHideBankNumber() {
  this.showBank = !this.showBank;
  if(this.showBank){
    this.bankdetailsElementRef.nativeElement.classList.add("passtxt");
    this.showhideElementRef.nativeElement.classList.remove("fa-eye-slash");
    this.showhideElementRef.nativeElement.classList.add("fa-eye");
  }
  else{
    this.bankdetailsElementRef.nativeElement.classList.remove("passtxt");
    this.showhideElementRef.nativeElement.classList.add("fa-eye-slash");
    this.showhideElementRef.nativeElement.classList.remove("fa-eye");
  }
}

  saveBankdetails(): void {
    this.submitted = true;
    // if (this.bankDetailsForm.valid && this.showGallary)
    if (this.bankDetailsForm.valid) {
          this.loading = true;
        const bankFormdata = JSON.stringify(this.bankDetailsForm.value);
        const bankDetails = JSON.parse(bankFormdata);
        let key = 'ifsc';
        let item = JSON.parse(localStorage.getItem(key));
        var object = {}
        object ['account_number'] = bankDetails.account_number;
        object ['address'] = item.data.bank_details.ADDRESS;
        object ['branch'] = item.data.bank_details.BRANCH;
        object ['ifsc'] = item.data.bank_details.IFSC;
        object ['name'] = item.data.bank_details.BANK;
        object ['micr'] = item.data.bank_details.MICR;
        object ['bank_code'] = item.data.bank_details.BANK_CODE;
        object ['bank_mode'] = item.data.bank_details.BANK_MODE;
        object ['dataInbase64'] = this.imageUrl;
        
        if(this.loginflag === true)
        object ['default'] = "N";
        else
        object ['default'] = "Y";

        if(this.sipPaymentFlag === true && this.loginflag === true){
          object ['default'] = "Y";
        }
        object ['type'] = bankDetails.type;
        
        this.apiService.storeBankdetails(object).subscribe(
              res  => {
                this.loading = false;
                if(this.sipPaymentFlag === true && this.loginflag === true){
                  this.bankDetails();
                  return;
                }
                if(this.loginflag === true && this.status >= '50'){
                  this.router.navigate(['/show-bank']);
                }
                else{
                  let account_status = 'account_status';
                  localStorage.setItem(account_status, '50');
                  this.router.navigate(['/signaturepad-verification']);
                }

                  },
              (error) => {
                  this.loading = false;
                  this.errorHaldlingService.setFieldValidators(error, this.bankDetailsForm);
                  this.errorStatus = error.status;
                if(this.errorStatus != "400" && this.errorStatus != "444"){
                  this.otherError = true;
                  this.errorMessage = error.error.data;
                  setTimeout(function() {
                    this.otherError = false;
                }.bind(this), 3000);
                }
                else {
                  this.ocrError = true;
                  this.ocrErrormsg = error.error.data;
                }
                  }
          )
    }
  }

/*get bank details*/
  bankDetails(){
    this.apiService.getBankDetails().subscribe(
      res => {
        this.banklist = res.data.bank_details;
        this.bankMandate = res.data.mandate_all;
        this.banklist.forEach(row => {
          this.rowsControls.push({
            isCollapsed: true
          })
        });
        let key = "bank_account"
        localStorage.setItem(key, JSON.stringify(this.banklist));
        localStorage.setItem("allBankMandate", JSON.stringify(this.bankMandate));

        localStorage.removeItem('sip_pay_addBank');
        this.router.navigate(['/sip-payment']);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      
      }
    );
    /*get bank details*/
  }

}
