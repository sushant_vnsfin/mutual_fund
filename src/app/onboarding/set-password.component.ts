import { Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch, CustomValidators, NoWhitespaceValidator } from '../helpers/must-match.validator';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-word',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {
  setPasswordForm: FormGroup;
  showBank = true;
  loading = false;
  showError = true;
  submitted = false;
  errorMessage = '';
  otherError = false;
  flag:boolean; 
  loginflag:boolean; 
  flagkey = 'forgotFlag';
  loginflagkey = 'loginflag';
  errorStatus = '';
  passApi: Observable<any>;
  modalReference: NgbModalRef;
  authToken: string;
  status: string;
  fundsname: any;
  @ViewChild('bankdetailsRef') bankdetailsElementRef: ElementRef;
  @ViewChild('showhideRef') showhideElementRef: ElementRef;

  constructor(private fb: FormBuilder,  private http: HttpClient, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation, private modalService: NgbModal) {
    location.onPopState((event) => {
      // Remove flag to avoid multiple buttons at set-password page
      localStorage.removeItem(this.loginflagkey);
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    this.flag = JSON.parse(localStorage.getItem(this.flagkey));
    this.loginflag = JSON.parse(localStorage.getItem(this.loginflagkey));
    /*check auth token and go to the home page*/
      let key = 'authentication_token';
      this.authToken = localStorage.getItem(key)
      let account_status = 'account_status';
      this.status = localStorage.getItem(account_status);
      if(this.authToken !== null && this.status >= '10'){
        if(this.flag === false){ 
        }
        else{
          this.loading = true;
          this.router.navigate(['/home']);
        }
      }
    /*check auth token and go to the home page*/


    this.setPasswordForm = this.fb.group({
      confPassword: ['', [Validators.required, NoWhitespaceValidator.cannotContainSpace]],
      password: ['', [Validators.required,
            // check whether the entered password has a number
            CustomValidators.patternValidator(/\d/, {hasNumber: true}),
            // check whether the entered password has upper case letter
            CustomValidators.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
            // check whether the entered password has a lower case letter
            CustomValidators.patternValidator(/[a-z]/, {hasSmallCase: true}),
            // check whether the entered password has a special character
            CustomValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,{hasSpecialCharacters: true}), Validators.minLength(8), NoWhitespaceValidator.cannotContainSpace]]
    }, {
           validator: MustMatch('password', 'confPassword')
        });

  }


  // convenience getter for easy access to form fields
  get f() { return this.setPasswordForm.controls; }



    // Function to open logout modal
    setPasswordPopup(setPassword) {
      this.modalReference = this.modalService.open(setPassword, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
    }

/*show and hide password format for text field*/
showOrHideBankNumber() {
  this.showBank = !this.showBank;
  if(this.showBank){
    this.bankdetailsElementRef.nativeElement.classList.add("passtxt");
    this.showhideElementRef.nativeElement.classList.remove("fa-eye-slash");
    this.showhideElementRef.nativeElement.classList.add("fa-eye");
  }
  else{
    this.bankdetailsElementRef.nativeElement.classList.remove("passtxt");
    this.showhideElementRef.nativeElement.classList.add("fa-eye-slash");
    this.showhideElementRef.nativeElement.classList.remove("fa-eye");
  }
}
/*show and hide password format for text field*/


  /*autofocus*/
  ngAfterViewInit(){
      this.bankdetailsElementRef.nativeElement.focus();
  }
  /*autofocus*/

/*show and hide password policy*/
onSearchChange(searchValue: string): void {
 if(this.f.password.invalid){
 this.showError = false;
 }
 else{
  this.showError = true;
 }
}
/*show and hide password policy*/


  savePassword(): void {
    this.submitted = true;
    if (this.setPasswordForm.valid) {
        this.loading = true;
        const faFormdata = JSON.stringify(this.setPasswordForm.value);
        const answerdata = JSON.parse(faFormdata);
        var object = {};
        object ['password'] = ""+ answerdata.password + "";
        
        
       /*check password api*/
   if((this.flag === true) || (this.flag === false)){
    if((this.flag != true)){
      let key = 'changeEmail';
      let item = localStorage.getItem(key);
      object ['email'] = item;
    }
    else {
      let key = 'changeEmail';
      let item = JSON.parse(localStorage.getItem(key));
      object ['email'] = item.email;
    }
     this.apiService.resetPassword(object).subscribe(
      res  => {
        this.loading = false;
        if((this.flag != true)){
          let element: HTMLElement = document.getElementById('setPassword') as HTMLElement;
          element.click();

          setTimeout(() => {
            this.loading = false;
            let key = 'authentication_token';
            localStorage.setItem(key, res.data.authentication_token);
            this.modalReference.close();
            this.router.navigate(['/home']);
          }, 1000);
        }
        else {
          let element: HTMLElement = document.getElementById('setPassword') as HTMLElement;
          element.click();
          setTimeout(() => {
            this.loading = false;
            this.modalReference.close();
            this.router.navigate(['/login']);
          }, 3000);
        
        }
        
          },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setFieldValidators(error, this.setPasswordForm);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
          }
  )
     
    }
    
    else{
      let key = 'email';
      let item = JSON.parse(localStorage.getItem(key));
      object ['email'] = item.email;
     this.apiService.setPassword(object).subscribe(
      res  => {

        let key = 'authentication_token';
        localStorage.setItem(key, res.data.authentication_token);
        let account_status = 'account_status';
        localStorage.setItem(account_status, '10');
        /*Start fund list api*/
        this.apiService.getFundName().subscribe(
          res  => {
              this.fundsname = res.data;
              let fund_list = 'fund_list';  
                localStorage.setItem(fund_list, JSON.stringify(this.fundsname));
                this.loading = false;
                this.router.navigate(['/mobile-verification']);
              },
          (error) => {
            
          }
      )
        /*End fund list api*/
          },
      (error) => {
              this.loading = false;
              this.errorHaldlingService.setFieldValidators(error, this.setPasswordForm);
              this.errorStatus = error.status;
              if(this.errorStatus != "400") {
                this.otherError = true;
                this.errorMessage = error.error.data;
                setTimeout(function () {
                  this.otherError = false;
                }.bind(this), 3000);
              }
       
          }
  )
    }
    /*check password api*/
  }

}


}
