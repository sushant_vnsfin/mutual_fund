import {  Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { RelationSelect } from '../helpers/must-match.validator';
import { MbscDatetimeOptions, mobiscroll } from '@mobiscroll/angular';
import {Router} from '@angular/router';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { ApiServiceService } from '../helpers/api-service.service';
import { DOCUMENT, DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})



export class PersonalDetailsComponent implements OnInit {
  now = new Date();
  bday = new Date(this.now.getFullYear() - 18, this.now.getMonth(), this.now.getDay());
  personaldetailsForm: FormGroup;
  submitted = false;
  marked = false;
  flag = 0;
  loading = false;
  otherError = false;
  errorMessage = '';
  errorStatus = '';
  relationship : any;
  dob: string;
  username: any;
  dobirth:any;
  authToken: string;
  status: string;
  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, @Inject(DOCUMENT) private document: Document, private errorHaldlingService: ErrorHaldlingService, public datepipe: DatePipe,  public location: Location) { 
    this.getPersonalDetail();
  }

  nomineeCheckbox: FormControl = new FormControl();


dateSettings: MbscDatetimeOptions = {
    dateFormat: 'yy-mm-dd',
     max: this.bday
};

  ngOnInit() {
        /*check auth token and go to the home page*/
        let auth_key = 'authentication_token';
        this.authToken = localStorage.getItem(auth_key)
        let account_status = 'account_status';
        this.status = localStorage.getItem(account_status);
        if(this.authToken !== null && this.status >= '40'){
          this.loading = true;
          this.router.navigate(['/home']);
        }
        /*check auth token and go to the home page*/

    //Get relationship list.
    this.relationship = [
      { "id": 1, "rname": "Wife", "value": "wife" },
      { "id": 2, "rname": "Brother", "value": "brother" },
      { "id": 3, "rname": "Father", "value": "father" },
      { "id": 4, "rname": "Mother", "value": "mother" },
      { "id": 5, "rname": "Husband", "value": "husband" },
    ];


    this.personaldetailsForm = this.fb.group({
      toggle: false,
      name: ['',  [Validators.required]],
      gender: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      nominee_name: [{value: null, disabled: true}, []],
      nominee_relation: [{value: "Select Your Relation", disabled: true}, [RelationSelect]]
    }
  );
this.setUserCategoryValidators();
}

getPersonalDetail(){
  this.apiService.getPersonaldetails().subscribe(
    res  => {
        this.username = res.data.user_details.name;
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.personaldetailsForm);
          this.errorStatus = error.status;
          if(this.errorStatus != '400'){
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function() {
              this.otherError = false;
          }.bind(this), 3000);
          }
            }
        );
}


setUserCategoryValidators() {
    const nomineenameControl = this.f.nominee_name;
    const relationnameControl = this.f.nominee_relation;

    this.nomineeCheckbox.valueChanges.subscribe(value => {
        if (value == true) {
          this.flag = 1;
           nomineenameControl.setValidators([Validators.required,Validators.pattern(/^[a-zA-Z][a-zA-Z ]+[a-zA-Z]*$/)]);
        }
        if (value == false){
          this.flag = 0;
           nomineenameControl.setValidators(null);
        }
        nomineenameControl.updateValueAndValidity();

      });
  }


onChange() {
this.nomineeCheckbox.valueChanges.subscribe(value => {
  this.marked= value;
if (value == true) {
  this.flag = 1;
this.personaldetailsForm.get('nominee_name').enable();
this.personaldetailsForm.get('nominee_relation').enable();
} else {
  this.flag = 0;
this.personaldetailsForm.get('nominee_name').disable();
this.personaldetailsForm.get('nominee_name').reset();
this.personaldetailsForm.get('nominee_relation').disable();
}
});
}

// convenience getter for easy access to form fields
get f() { return this.personaldetailsForm.controls; }

/*personal details function*/
savePersonaldetails(): void {
  this.submitted = true;
  if (this.personaldetailsForm.valid) {
        this.loading = true;
      const personalDetailsFormdata = JSON.stringify(this.personaldetailsForm.value);
      const pDetails = JSON.parse(personalDetailsFormdata);
      let dob = new Date(pDetails.dob);
      let latest_date =this.datepipe.transform(dob, 'yyyy-MM-dd');
      
      var object = {};
      object ['name'] = pDetails.name;
      object ['gender'] = pDetails.gender;
      object ['dob'] = latest_date;
      object ['nominee_relation'] = pDetails.nominee_relation;
      object ['nominee_flag'] = this.flag;
      if(this.flag === 0){
        object ['nominee_name'] = "";
        object ['nominee_relation'] = "";
      }else{
        object ['nominee_name'] = pDetails.nominee_name;
        object ['nominee_relation'] = pDetails.nominee_relation;
      }

      this.apiService.personalDetails(object).subscribe(
            res  => {
              this.router.navigate(['/ifsc-code']);
              let account_status = 'account_status';
              localStorage.setItem(account_status, '40');
                },
            (error) => {
              this.loading = false;
              this.errorHaldlingService.setFieldValidators(error, this.personaldetailsForm);
              this.errorStatus = error.status;
              if(this.errorStatus != '400'){
                this.otherError = true;
                this.errorMessage = error.error.data;
                setTimeout(function() {
                  this.otherError = false;
              }.bind(this), 3000);
              }
                }
        )


  }
}
}
