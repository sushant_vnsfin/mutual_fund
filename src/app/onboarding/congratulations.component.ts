import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-congratulations',
  templateUrl: './congratulations.component.html',
  styleUrls: ['./congratulations.component.css']
})
export class CongratulationsComponent implements OnInit {

  constructor(private router: Router,  private apiService: ApiServiceService) { 
    this.detailsUser();
  }

  ngOnInit() {
    
  }

  detailsUser(){
    this.apiService.getUserdetails().subscribe(
      res  => {
        let account_status = 'account_status';
        localStorage.setItem(account_status, res.data.account_status);
        });
  }

}
