import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PlatformLocation } from '@angular/common';
import { countDownTimerConfigModel, CountdownTimerService, countDownTimerTexts } from 'ngx-timer';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})

export class ResetpasswordComponent implements OnInit {
  resetPassForm: FormGroup;
  verifyOTP: FormGroup;
  submitted = false;
  otpsubmitted = false;
  key = 'forgotFlag';
  ekey = 'changeEmail';
  flag = false;
  resend = true;
  resendF = false;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  setemail: any;
  otherError = false;
  testConfig: countDownTimerConfigModel;
  userEmail: any;

  constructor(private fb: FormBuilder, public location: PlatformLocation, private errorHaldlingService: ErrorHaldlingService, private countdownTimerService: CountdownTimerService, private router: Router, private apiService: ApiServiceService) {
    location.onPopState((event) => {
      // Remove flag to avoid multiple buttons at set-password page
    });
  }

  ngOnInit() {
    this.flag = JSON.parse(localStorage.getItem(this.key));
    if (this.flag === false) {
      this.setemail = localStorage.getItem(this.ekey);
    }
    this.resetPassForm = this.fb.group({
      email: ['', [Validators.required]],
    });
    this.verifyOTP = this.fb.group({
      otp: ['', [Validators.required]],
    });


    this.testConfig = new countDownTimerConfigModel();
    this.testConfig.timerClass = 'test_Timer_class';
    this.testConfig.timerTexts = new countDownTimerTexts();
    this.testConfig.timerTexts.hourText = " :"; //default - hh
    this.testConfig.timerTexts.minuteText = " :";
    this.testConfig.timerTexts.secondsText = " ";

    this.countdownTimerService.onTimerStatusChange.subscribe(status => {
      if (status = "STOP") {
        this.resend = true;
        this.countdownTimerService.resumeTimer();
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetPassForm.controls; }
  get fo() { return this.verifyOTP.controls; }

  startTimerGT() {
    let cdate = new Date();
    cdate.setMinutes(cdate.getMinutes() + 2);
    cdate.setSeconds(cdate.getSeconds() + 0);
    this.countdownTimerService.startTimer(cdate);
  }

  resumeTimer() {
    this.countdownTimerService.resumeTimer();
  }

  saveResetPass(): void {
    this.submitted = true;
    if (this.resetPassForm.valid) {
      this.otherError = false;
      this.loading = true;
      const resetFormdata = JSON.stringify(this.resetPassForm.value);
      this.userEmail = JSON.parse(resetFormdata);
      var object = {};
      let forgotkey = 'forgotFlag';
      let flagitem = localStorage.getItem(forgotkey);
      object['email'] = this.userEmail.email;
      object['flag'] = flagitem;
      this.apiService.restPassword(object).subscribe(
        res => {
          this.loading = false;
          this.resendF = true;
          this.flag = JSON.parse(localStorage.getItem(this.key));
          if (this.flag != false) {
            localStorage.setItem(this.ekey, JSON.stringify(this.userEmail));
          }
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.resetPassForm);
          this.errorStatus = error.status;
          // if (this.errorStatus == "403") {
          //   localStorage.clear();
          //   this.router.navigate(['/login']);
          // }
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
    else {

    }

  }


  resendOTP(): void {
      var object = {};
      let forgotkey = 'forgotFlag';
      let flagitem = localStorage.getItem(forgotkey);
      object['email'] = this.userEmail.email;
      object['flag'] = flagitem;
    this.startTimerGT();
    this.resend = false;
    this.apiService.restPassword(object).subscribe(res => { });
  }


  sendOTP(): void {
    this.otpsubmitted = true;
    if (this.verifyOTP.valid) {
      this.otherError = false;
      this.loading = true;
      const resetOTPData = JSON.stringify(this.verifyOTP.value);
      const resetOTP = JSON.parse(resetOTPData);
      var object = {};
      let key = 'changeEmail';
      let forgotkey = 'forgotFlag';

      let flagitem = localStorage.getItem(forgotkey);
      if (flagitem != 'true') {
        let item = localStorage.getItem(key);
        object['email'] = item;
      }
      else {
        let item = JSON.parse(localStorage.getItem(key));
        object['email'] = item.email;
      }


      object['otp'] = resetOTP.otp;
      this.apiService.otpVerification(object).subscribe(
        res => {
          this.loading = false;
          this.router.navigate(['/set-password']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.resetPassForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )


    }
    else {

    }

  }
}
