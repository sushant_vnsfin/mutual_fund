import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-viewdetails',
  templateUrl: './viewdetails.component.html',
  styleUrls: ['./viewdetails.component.css']
})
export class ViewdetailsComponent implements OnInit {
  status: string;
  email: any;
  name: any;
  mobile: any;
  user: any;
  account: any;
  loading: boolean;
  modalReference: NgbModalRef;
  errorStatus = '';

  constructor(public location: PlatformLocation, private router: Router, private apiService: ApiServiceService, private modalService: NgbModal, private errorHaldlingService: ErrorHaldlingService) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    /*account status*/
    let key = 'account_status';
    this.status = localStorage.getItem(key);
    /*account status*/

    /*account details fetched from home page*/
    let acc_key = 'acc_details';
    this.account = JSON.parse(localStorage.getItem(acc_key));
    /*account details fetched from home page*/

        this.user = this.account.user_details;
        this.email = this.account.email;
        if (this.user == null) {
          this.name = '';
          this.mobile = '';
        }
        else {
          this.name = this.account.user_details.name;
          this.mobile = this.account.user_details.mobile;
        }
        let ekey = 'email';
        var object = {};
        object['email'] = this.account.email;
        localStorage.setItem(ekey, JSON.stringify(object));
  }

  // Function to open logout modal
  logoutPopup(logout) {
    this.modalReference = this.modalService.open(logout, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }

  completeProfile(): void {
    var pagestatus = this.status;
    switch (pagestatus) {
      case "10": {
        this.router.navigate(['/mobile-verification']);
        break;
      }
      case "20": {
        this.router.navigate(['/pan-number']);
        break;
      }
      case "30": {
        this.router.navigate(['/personal-details']);
        break;
      }
      case "35": {
        this.router.navigate(['/fatca-details']);
        break;
      }
      case "40": {
        this.router.navigate(['/ifsc-code']);
        let key = 'bankflag';
        localStorage.setItem(key, JSON.stringify(false));
        break;
      }
      default: {
        this.router.navigate(['/signaturepad-verification']);
        break;
      }
    }
  }

  /*Log Out*/
  logOut(): void {
    this.loading = true;
    this.apiService.logOut().subscribe(
      res => {
        localStorage.clear();
        this.loading = false;
        this.router.navigate(['/login']);
      });
    (error) => {
      this.loading = false;
      this.errorHaldlingService.setGlobalValidators(error);
    }
  }
  /*Log Out*/

}
