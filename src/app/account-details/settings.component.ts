import { Component, OnInit } from '@angular/core';
import { MbscFormOptions } from '@mobiscroll/angular';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  switch1 = false;
  switch2 = false;
  flag = "false";
  loading = false;
  errorStatus = '';

  constructor(private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: Location) { }

  ngOnInit() {
  }

  changed() {
  }

  /*change Password*/
  changePassword(): void {
    let key = 'forgotFlag';
    localStorage.setItem(key, this.flag);
    this.apiService.changePassword().subscribe(
      res => {
        let ekey = 'changeEmail';
        localStorage.setItem(ekey, res.data.email);
        this.router.navigate(['/reset-password']);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }
  /*change Password*/
}
