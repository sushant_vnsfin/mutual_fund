import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-bankmandatedetails',
  templateUrl: './bankmandatedetails.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./bankmandatedetails.component.css']
})

export class BankmandatedetailsComponent implements OnInit {
  flag: boolean;
  marked: boolean;
  viewbankdetails: [];
  closeResult: string;
  remBtn = true;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;
  bankDetails: any;
  modalReference: NgbModalRef;
  viewkey = 'bankDetails';
  lenkey = 'bankLenth';
  key = 'activeFlag';

  constructor(public location: PlatformLocation,public router: Router, private modalService: NgbModal, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { 
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    let flag = JSON.parse(localStorage.getItem(this.key));
    let bankLen = JSON.parse(localStorage.getItem(this.lenkey));
    this.bankDetails = JSON.parse(localStorage.getItem(this.viewkey));
    this.viewbankdetails = this.bankDetails.data;
    let bankDefault = this.bankDetails.data[0].default;

    if (bankLen == 1 || bankDefault == 'Y') {
      this.remBtn = false;
    }
    else {
      this.remBtn = true;
    }
    if (flag === true) {
      this.marked = true;
    }
    else {
      this.marked = false;
    }
    localStorage.removeItem(this.key);
  }

  removeBankModal(content) {
    this.modalReference =  this.modalService.open(content, { centered: true, windowClass: 'in remove-bank-popup', backdropClass: 'change-modal-backdrop' });
  }

  /*Remove Bank Account*/
  removeBank() {
    this.loading = true;
    var object = {}
    object['account_number'] = this.bankDetails.data[0].account_number;
    this.apiService.removeBank(object).subscribe(
      res => {
        this.loading = false;
        this.router.navigate(['/show-bank']);
        let keysToRemove = ["bankDetails", "bankLenth"];
        keysToRemove.forEach(k => localStorage.removeItem(k))
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
      }
    )
  }
  /*Remove Bank Account*/

}
