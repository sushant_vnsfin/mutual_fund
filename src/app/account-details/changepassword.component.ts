import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  @ViewChild('changePassRef') changePassElementRef: ElementRef;

  constructor(public location: Location) { }

  ngAfterViewInit() {
    this.changePassElementRef.nativeElement.focus();
  }

  ngOnInit() {
  }

  changePassword(): void {

  }

}
