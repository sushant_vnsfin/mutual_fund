import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})

export class AccountDetailsComponent implements OnInit {
  accountdetailsForm: FormGroup;
  relationship: any;
  username: any;
  dob: any;
  pan: any;
  mobile: any;
  email: any;
  n_name: any;
  n_rel: any;
  status: any;
  gender: any;
  account: any;
  nomineeSec = false;
  myCheckbox: FormControl = new FormControl();

  constructor(private fb: FormBuilder, private apiService: ApiServiceService, public location: Location) { }

  ngOnInit() {
    /*account status*/
    let key = 'account_status';
    this.status = localStorage.getItem(key);
    /*account status*/

    /*account details fetched from home page*/
    let acc_key = 'acc_details';
    this.account = JSON.parse(localStorage.getItem(acc_key));
    /*account details fetched from home page*/

    if (this.status < '40') {
      this.username = '';
      this.dob = '';
      this.gender = '';
    }
    else {
      this.username = this.account.user_details.name;
      this.dob = this.account.user_details.dob;
      this.gender = this.account.user_details.gender;
      if (this.account.nominees === null) {
        this.relationship = '';
        this.nomineeSec = false;
        this.n_rel = '';
        this.n_name = '';
      }
      else {
        this.relationship = this.account.nominees;
        this.nomineeSec = true;
        this.n_rel = this.account.nominees.relation;
        this.n_name = this.account.nominees.name;
      }
    }

    if (this.status < '30') {
      this.pan = '';
    }
    else {
      this.pan = this.account.user_details.pan;
    }

    if (this.status < '20') {
      this.mobile = '';
    }
    else {
      this.mobile = this.account.user_details.mobile;
    }

    if (this.status === '10') {
      this.email = this.account.email;
    }
    else {
      this.email = this.account.email;
    }

    this.accountdetailsForm = this.fb.group({
      toggle: false,
      name: [''],
      dob: [''],
      mobileno: [''],
      email: [''],
      pancard: [''],
      gender: [''],
      nomineeName: [''],
      relationName: ['']
    }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.accountdetailsForm.controls; }

}
