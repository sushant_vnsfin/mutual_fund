import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ApiServiceService {
  auth: any = '';
  headers: any ;

  constructor(private http: HttpClient) {
  }

  commonHeaderFunction(){
    let key = 'authentication_token';
    this.auth = localStorage.getItem(key);
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.auth
      })
    };
  }

    baseURL: string = environment.APIEndpoint;
  // baseURL: string = 'http://192.168.27.100/mutualfund_api/public/';
  // baseURL: string = 'http://192.168.27.39/mutualfund_api/public/';


  apiURL: string = this.baseURL + 'api/';

  
  getVerison(): Observable<any> {
    return this.http.get(this.apiURL + "get-app-version").pipe(map((res: any) => res));
  }
  
  setPassword(object): Observable<any> {
    return this.http.post(this.apiURL + "register", object).pipe(map((res: any) => res));
  }

  resetPassword(object): Observable<any> {
    return this.http.post(this.apiURL + "update-password", object).pipe(map((res: any) => res));
  }

  emailCheck(object): Observable<any> {
    return this.http.post(this.apiURL + "check-email", object).pipe(map((res: any) => res));
  }

  insertPan(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "insert-pan", object, this.headers).pipe(map((res: any) => res));
  }


  personalDetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "store-personaldetails", object, this.headers).pipe(map((res: any) => res));
  }
  
  fatcaDetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-fatca", object, this.headers).pipe(map((res: any) => res));
  }

  
  mobileVerification(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "verify-mobile", object, this.headers).pipe(map((res: any) => res));
  }


  otpVerification(object): Observable<any> {
    return this.http.post(this.apiURL + "verify-otp", object).pipe(map((res: any) => res));
  }


  resendotpVerification(object): Observable<any> {
    return this.http.post(this.apiURL + "send-otp", object).pipe(map((res: any) => res));
  }


  getBankDetails(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-allbanks", this.headers).pipe(map((res: any) => res));
  }

  setBankDefault(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.put(this.apiURL + "set-defaultbank", object, this.headers).pipe(map((res: any) => res));
  }


  viewBankDetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-bankdetails", object, this.headers).pipe(map((res: any) => res));
  }


  storeBankdetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "store-bankdetails", object, this.headers).pipe(map((res: any) => res));
  }

  signStore(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "store-signature", object, this.headers).pipe(map((res: any) => res));
  }


  getBankifsc(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-bankdetails-ifsc", object, this.headers).pipe(map((res: any) => res));
  }


  saveBankifsc(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "store-bankdetails", object, this.headers).pipe(map((res: any) => res));
  }

  getBanklist(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-banklist", this.headers).pipe(map((res: any) => res));
  }


  changePassword(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "reset-password", this.headers).pipe(map((res: any) => res));
  }

  getPersonaldetails(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-personaldetails", this.headers).pipe(map((res: any) => res));
  }


  getBranchlist(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-branchlist", object, this.headers).pipe(map((res: any) => res));
  }


  removeBank(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "remove-bank", object, this.headers).pipe(map((res: any) => res));
  }


  addMandate(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "add-mandate", object, this.headers).pipe(map((res: any) => res));
  }

  resendMandate(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "send-mandateemail", object, this.headers).pipe(map((res: any) => res));
  }

  getFundName(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-mfschemelist", this.headers).pipe(map((res: any) => res));
  }

  getOrderHistory(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-order-history", this.headers).pipe(map((res: any) => res));
  }

  getOrderDetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-order-details", object, this.headers).pipe(map((res: any) => res));
  }


  getFundDetails(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-fund-details", object, this.headers).pipe(map((res: any) => res));
  }




  getMFDates(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-mfschemedates", object, this.headers).pipe(map((res: any) => res));
  }

  uploadMandate(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "upload-signed-mandate", object, this.headers).pipe(map((res: any) => res));
  }

  proceedSip(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "place-order", object, this.headers).pipe(map((res: any) => res));
  }

  paymentOrder(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "confirm-payment-order", object, this.headers).pipe(map((res: any) => res));
  }


  login(object): Observable<any> {
    return this.http.post(this.apiURL + "login", object).pipe(map((res: any) => res
    ));
    
  }

  restPassword(object): Observable<any> {
    return this.http.post(this.apiURL + "forgot-password", object).pipe(map((res: any) => res));
  }


  getUserdetails(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-userdetails", this.headers).pipe(map((res: any) => res));
  }

  showPanName(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "pan-name-check", object, this.headers).pipe(map((res: any) => res));
  }


  logOut(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "logout", this.headers).pipe(map((res: any) => res));
  }


  getNavChart(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-nav-chart", object, this.headers).pipe(map((res: any) => res));
  }

  getPortfolioDetails(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-portfolio", this.headers).pipe(map((res: any) => res));
  }

  getFundTransactions(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-fund-transactions", object, this.headers).pipe(map((res: any) => res));
  }

  getPortfolioAnalysis(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-portfolio-analysis", this.headers).pipe(map((res: any) => res));
  }

  getRedeemAmount(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-redeemable-amount", object, this.headers).pipe(map((res: any) => res));
  }

  getSchemeEntryExit(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-scheme-entry-exit", object, this.headers).pipe(map((res: any) => res));
  }

  getSip(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-sip", this.headers).pipe(map((res: any) => res));
  }

  getSipTransaction(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-sip-transactions", object, this.headers).pipe(map((res: any) => res));
  }

  getUpdatedMandateId(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "update-mandateid", object, this.headers).pipe(map((res: any) => res));
  }


  confirmOrder(object): Observable<any> {
    this.commonHeaderFunction();
    // return this.http.post(this.apiURL + "confirm-payment-status", object, this.headers).pipe(map((res: any) => res));
    return this.http.post(this.apiURL + "check-payment-response-from-bse", object, this.headers).pipe(map((res: any) => res));
  }


  cancelSIP(object): Observable<any> {
    this.commonHeaderFunction();
    // return this.http.post(this.apiURL + "confirm-payment-status", object, this.headers).pipe(map((res: any) => res));
    return this.http.post(this.apiURL + "cancel-sip", object, this.headers).pipe(map((res: any) => res));
  }

  getRiskSchemes(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-risk-schemes", object, this.headers).pipe(map((res: any) => res));
  }

  getTopFunds(): Observable<any> {
    this.commonHeaderFunction();
    return this.http.get(this.apiURL + "get-top-fund", this.headers).pipe(map((res: any) => res));
  }

  getFundByCategory(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-fund-by-category", object, this.headers).pipe(map((res: any) => res));
  }

  getSchemesByInvestment(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-schemes-by-investment", object, this.headers).pipe(map((res: any) => res));
  }

  getMinRedeemAmount(object): Observable<any> {
    this.commonHeaderFunction();
    return this.http.post(this.apiURL + "get-mfschememinredeemamount", object, this.headers).pipe(map((res: any) => res));
  }

}
