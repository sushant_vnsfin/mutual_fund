import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ImageService {

  constructor(private httpClient: HttpClient) {}


  public uploadImage(fileToUpload: File) {
    const formData: FormData = new FormData();

    formData.append('image', fileToUpload);

    return this.httpClient.post('/api/image-upload/', formData);
  }
}
