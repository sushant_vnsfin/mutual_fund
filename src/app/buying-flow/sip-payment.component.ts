import { Component, OnInit, Inject, SecurityContext, HostListener} from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { interval, Subscription } from 'rxjs';
import { countDownTimerConfigModel, CountdownTimerService, countDownTimerTexts } from 'ngx-timer';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-sip-payment',
  templateUrl: './sip-payment.component.html',
  styleUrls: ['./sip-payment.component.css']
})

export class SipPaymentComponent implements OnInit {
  sippaymentForm: FormGroup;
  modalReference: NgbModalRef;
  monthly_date: any;
  scheme_amount: any;
  banklist:any = [];
  rowsControls = [];
  scheme_details: any;
  submitted = false;
  bankdef = "1";
  loading = false;
  paymentLoad = false;
  errorMessage = '';
  errorStatus = '';
  otherError = false;
  investment_type: any;
  URL: string;
  intervalId: number;
  subscription: Subscription;
  conorder: boolean = false;
  myWindow: Window;
  resend = true;
  id: any;
  bank: any;
  bankID: any;
  prev_ur: string;
  testConfig: countDownTimerConfigModel;
  loginflag = true;

  constructor(@Inject(DOCUMENT) private document: Document, public location: PlatformLocation, private apiService: ApiServiceService, private fb: FormBuilder, private sanitizer: DomSanitizer, private router: Router, private inBrowser: InAppBrowser,private modalService: NgbModal, private countdownTimerService: CountdownTimerService, private errorHaldlingService: ErrorHaldlingService, private routerService: RouterExtService) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
    
  }

  ngOnInit() {
    const prev_url = this.routerService.getPreviousUrl();
    console.log(prev_url);
    if(prev_url != "/fund-details" && prev_url != "/purchase-sip" && prev_url != "/sip-date" &&  prev_url != "/sip-payment" &&  prev_url != "/bank-details"){
      this.router.navigate(['/home']);
   }

    // fetch bank details from localStorage
    let bankAccounts = "bank_account"
    this.banklist = JSON.parse(localStorage.getItem(bankAccounts));
    
      for (var i = 0; i <= this.banklist.length - 1; i++) {
        if(this.banklist[i].default == 'Y'){
         this.bankID = this.banklist[i].id;
      }
    }
    
    this.sippaymentForm = this.fb.group({
      bankSip: [this.bankID, [Validators.required]],
    });


    /*get month date*/
    let key = 'monthly_date';
    this.monthly_date = JSON.parse(localStorage.getItem(key));
    let scheme_details = 'scheme_details';
    this.scheme_details = JSON.parse(localStorage.getItem(scheme_details));
    /*get month date*/

    /*get scheme info*/
    let scheme_info = 'scheme_info';
    this.scheme_amount = JSON.parse(localStorage.getItem(scheme_info));

    let investment_key = 'investment_type';
    this.investment_type = localStorage.getItem(investment_key);
    /*get scheme info*/
    /**/
    /**/

    this.testConfig = new countDownTimerConfigModel();
    this.testConfig.timerClass = 'test_Timer_class';
    this.testConfig.timerTexts = new countDownTimerTexts();
    this.testConfig.timerTexts.hourText = " :"; //default - hh
    this.testConfig.timerTexts.minuteText = " :";
    this.testConfig.timerTexts.secondsText = " ";
  }

  bankIdCap(bankIdDefault):void {
     this.bankID = bankIdDefault;
  }
  
  // convenience getter for easy access to form fields
  get f() { return this.sippaymentForm.controls; }

  sipPaymentPopup(payment) {
    this.modalReference = this.modalService.open(payment, { windowClass: 'in submit-otm-upload', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }

  startTimerGT() {
    let cdate = new Date();
    cdate.setMinutes(cdate.getMinutes() + 5);
    cdate.setSeconds(cdate.getSeconds() + 0);
    this.countdownTimerService.startTimer(cdate);
  }

  /*Function if payment popup closed by clicking on CANCEL*/
  closePaymentPop() {
    this.paymentLoad = false;
    this.subscription && this.subscription.unsubscribe();
    this.router.navigate(['/order-history']);
  }

  /*sip payment function*/
  // sipPayment1(): void {
  //   localStorage.removeItem('sip-successfl-flag');
  //   this.router.navigate(['/sip-success']);
  // }
   sipPayment(): void {
    this.submitted = true;
    const bank_id = JSON.stringify(this.sippaymentForm.value);
    const bankValue = JSON.parse(bank_id);
    let order_id = 'order_id';
    if (this.sippaymentForm.valid) {
      this.loading = true;
      var object = {};
      object['bank_id'] = this.bankID;
      object['amount'] = this.scheme_amount.amount;
      object['scheme_type'] = this.investment_type;
      object['order_id'] = localStorage.getItem(order_id);
      this.apiService.paymentOrder(object).subscribe(
        res => {
          this.URL = this.apiService.baseURL + 'payment-request/' + localStorage.getItem(order_id);
          this.myWindow = window.open(this.URL, "_blank", 'hideurlbar=yes');
          const source = interval(10000);
          this.subscription = source.subscribe(val => this.confirmpayment())

          /*open payment popup on opening of in-app browser*/
          this.paymentLoad = true;
          this.resend = false;
          this.startTimerGT();
          /*open payment popup on opening of in-app browser*/

          /*close payment function*/
          this.id = setTimeout(function () {
            this.myWindow.close();
            this.paymentLoad = false;
            this.router.navigate(['/order-history']);
            this.subscription && this.subscription.unsubscribe();
            this.countdownTimerService.stopTimer();
          }.bind(this), 300000);
          /*close payment function*/
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.sippaymentForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  };
  /*sip payment function*/

  /*confirm sip payment function*/
  confirmpayment() {
    let order_id = 'order_id';
    let investment_type = localStorage.getItem("investment_type");
    var object = {};
    object['order_id'] = localStorage.getItem(order_id);
    this.apiService.confirmOrder(object).subscribe(
      res => {
        if (res.data.bse_callback_response != 0) {
          // this.conorder;
          this.myWindow.close();
          if (this.investment_type != "sip") {
            this.paymentLoad = false;
            this.loading = false;
            this.subscription && this.subscription.unsubscribe();
            this.countdownTimerService.stopTimer();
            let element: HTMLElement = document.getElementById('open_mandate') as HTMLElement;
            element.click();
          }
          else {
            this.loading = true;
            this.paymentLoad = false;
            this.countdownTimerService.stopTimer();
            this.subscription && this.subscription.unsubscribe();
            setTimeout(function () {
              this.otherError = false;
              localStorage.removeItem('sip-successfl-flag');
              this.router.navigate(['/sip-success']);
            }.bind(this), 3000);
          }
        }
        // else {
        //   !this.conorder;
        // }
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.sippaymentForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
      }
    )
  }
  /*confirm sip payment function*/

  orderHistory(): void {
    this.modalReference.close();
    this.router.navigate(['/order-history']);
  }

  /*Add Bank Account*/
  addBank(): void {
    localStorage.setItem('sip_pay_addBank','true')
    let key = 'loginflag';
    this.router.navigate(['/ifsc-code'], { skipLocationChange: true });
    localStorage.setItem(key, JSON.stringify(this.loginflag));
    }
  /*Add Bank Account*/

  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
      clearTimeout(this.id);
    }
    if (this.modalReference !== undefined) {
      this.modalReference.close();
    }
  }

}
