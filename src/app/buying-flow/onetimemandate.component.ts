import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { ClickOutsideDirective } from '../helpers/outerclick.directive';

@Component({
  selector: 'app-onetimemandate',
  templateUrl: './onetimemandate.component.html',
  styleUrls: ['./onetimemandate.component.css']
})

export class OnetimemandateComponent implements OnInit {
  otmForm: FormGroup;
  submitted = false;
  otamount: any = '1,00,000';
  otmamt: any;
  bankdetails: any;
  loading: boolean;
  errorStatus: any;
  errorMessage: any;
  otherError: boolean;
  showTooltip: boolean;

  constructor(private fb: FormBuilder, private router: Router, private cdr: ChangeDetectorRef, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: Location) { }

  ngOnInit() {
    let key = 'account_details';
    this.bankdetails = JSON.parse(localStorage.getItem(key));
    this.otmamt = this.otamount.replace(/\,/g, "");
    this.otmForm = this.fb.group({
      otmAmount: ['', [Validators.required, Validators.min(500)]],
    });
  }
  showtooltip(): void {
    this.showTooltip = !this.showTooltip;
  }

  ClickOutside() {
    this.showTooltip = false;
  }

  // convenience getter for easy access to form fields
  get f() { return this.otmForm.controls; }

  // function to get value from button in input field
  getOtmAmount(innerText) {
    this.otmamt = innerText.replace(/\,/g, "");
    this.otamount = this.otmamt;
    this.cdr.detectChanges();
  }

  saveOtm(): void {
    this.submitted = true;
    // if (this.otmForm.valid) {
    this.loading = true;
    var object = {}
    object['account_number'] = this.bankdetails.account_number;
    object['amount'] = this.otmamt;
    this.apiService.addMandate(object).subscribe(
      res => {
        this.loading = false;
        let key = 'mandate_flag';
        localStorage.setItem(key, JSON.stringify(res.data.mandate_flag));
        let account_key = 'account_details';
        localStorage.setItem(account_key, JSON.stringify(res.data));
        this.router.navigate(['/otm-upload'], { skipLocationChange: true });
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setFieldValidators(error, this.otmForm);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          setTimeout(function() {
            this.otherError = false;
        }.bind(this), 3000);
          this.errorMessage = error.error.data;
        }
      }
    )


  }
  // }

}




