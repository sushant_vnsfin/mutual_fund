import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-sip-date',
  templateUrl: './sip-date.component.html',
  styleUrls: ['./sip-date.component.css']
})

export class SipDateComponent implements OnInit {
  pannumberForm: FormGroup;
  submitted = false;
  loading = false;
  errorMessage = '';
  errorStatus = '';
  otherError = false;
  sipdate: any;
  modalReference: NgbModalRef;
  selectedHero: any;
  sipflag = "false";
  sipdatedata: any;
  sipdateflag: any;
  sipdayflag: any;
  funds: any;
  sipdateSelect = "false";
  sipdate1: any;
  sip_scheme: any;
  attr: number;
  id: any;
  defaultDate: any;
  today: any;

  constructor(private fb: FormBuilder, private router: Router, private cdr: ChangeDetectorRef, private apiService: ApiServiceService, private modalService: NgbModal, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  sipdateForm: FormGroup;

  newarr = [];
  finaldatearr = [];
  arr = [{ id: 1, attr: 'st' }, { id: 2, attr: 'nd' }, { id: 3, attr: 'rd' }, { id: 4, attr: 'th' }, { id: 5, attr: 'th' }, { id: 6, attr: 'th' }, { id: 7, attr: 'th' }, { id: 8, attr: 'th' }, { id: 9, attr: 'th' }, { id: 10, attr: 'th' }, { id: 11, attr: 'th' }, { id: 12, attr: 'th' }, { id: 13, attr: 'th' }, { id: 14, attr: 'th' }, { id: 15, attr: 'th' }, { id: 16, attr: 'th' }, { id: 17, attr: 'th' }, { id: 18, attr: 'th' }, { id: 19, attr: 'th' }, { id: 20, attr: 'th' }, { id: 21, attr: 'st' }, { id: 22, attr: 'th' }, { id: 23, attr: 'rd' }, { id: 24, attr: 'th' }, { id: 25, attr: 'th' }, { id: 26, attr: 'th' }, { id: 27, attr: 'th' }, { id: 28, attr: 'th' }, { id: 29, attr: 'th' }, { id: 30, attr: 'th' }, { id: 31, attr: 'st' }];
  n = 7;

  ngOnInit() {
    var date = new Date();
    this.today = date.getDate();

    this.sipdateForm = this.fb.group({
      sipdate: ['', [Validators.required]],
    });

    /*SIP Scheme Info*/
    let schemeinfo = 'scheme_info';
    this.sip_scheme = JSON.parse(localStorage.getItem(schemeinfo));
    /*SIP Scheme Info*/

    /*scheme_details*/
    let schemekey = 'scheme_details';
    this.funds = JSON.parse(localStorage.getItem(schemekey));
    /*scheme_details*/

    /*sip date*/
    let key = 'scheme_dates';
    this.sipdatedata = JSON.parse(localStorage.getItem(key));
    this.selectedHero = this.sipdatedata[0];

    let flagkey = 'sipdate';
    this.sipdateflag = JSON.parse(localStorage.getItem(flagkey));
    let strArray = this.sipdatedata.map(function (x) {
      return parseInt(x, 10);
    });

    for (var i = 0; i <= this.arr.length - 1; i++) {
      if (strArray.includes(this.arr[i].id) && this.arr[i].id > this.today) {
        var obj = {
          id: this.arr[i].id,
          attr: this.arr[i].attr,
          flag: true
        };
        this.newarr.push(obj);
      } else {
        var obj = {
          id: this.arr[i].id,
          attr: this.arr[i].attr,
          flag: false
        };
        this.newarr.push(obj);
      }
    }
    /*sip date*/

    // Filter Available dates for SIP
    var filtered = this.newarr.filter(function (item) {
      return item.flag == true;
    });
    this.sipdate = filtered[0].id + filtered[0].attr;
  }

  sipDateSelect(date) {
    this.modalReference = this.modalService.open(date, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop' });
  }

  openSipConfirm(selectSip) {
    // this.modalReference.close();
    if (this.sipdateForm.valid) {
      this.sipdayflag = true;
      this.modalReference = this.modalService.open(selectSip, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.sipdateForm.controls; }

  /*Save Sip Date*/
  saveSipdate(): void {
    this.submitted = true;
  }
  /*Save Sip Date*/

  /*cancel SIP Date*/
  cancelSipdate(): void {
    if (this.sipdateSelect == "true") {
      this.sipdate = this.sipdate1;
    }
    // else {
    //   this.sipdate = this.newarr[0].id + this.newarr[0].attr;
    // }
  }

  /*ok SIP Date*/
  okSipDate(): void {
    this.sipdateSelect = 'true';
    this.sipdate = this.sipdate1;
    this.cdr.detectChanges();
  }

  cancelSipday(): void {
    this.sipdayflag = false;
  }
  /*cancel SIP Date*/

  onSelect(x): void {
     // Filter Available dates for SIP
     var filtered = this.newarr.filter(function (item) {
      return item.flag == true;
    });
    if (x.flag != true) {
      this.sipdate1 = filtered[0].id + filtered[0].attr;
      this.selectedHero = this.sipdatedata[0];
    }
    else{
      this.selectedHero = x.id;
      this.sipdate1 = x.id + x.attr;
    }

    this.sipdateSelect = 'false';
    this.cdr.detectChanges();
  }


  /*sip proceed*/
  proccedSip(): void {
    this.submitted = true;
    if (this.sipdateForm.valid) {
      this.loading = true;
      this.sip_scheme['sip_start_date'] = this.selectedHero;
      this.sip_scheme['order_type'] = "purchase";
      this.sip_scheme['scheme_name'] = this.funds.fund_details.SchemeName;
      let key = 'monthly_date';
      localStorage.setItem(key, JSON.stringify(this.sipdate));

      this.apiService.proceedSip(this.sip_scheme).subscribe(
        res => {
          this.loading = false;
          let sip_order = 'order_id';
          localStorage.setItem(sip_order, res.data.order_id);
          localStorage.removeItem('sip-successfl-flag');
          this.router.navigate(['/sip-payment']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.sipdateForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  }
  /*sip proceed*/
}
