import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-fund-transaction',
  templateUrl: './fund-transaction.component.html',
  styleUrls: ['./fund-transaction.component.css']
})

export class FundTransactionComponent implements OnInit {
  order: any;
  fundDetails: any;
  portfolio: any;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;
  minRedeemAmt: any;

  constructor(private router: Router, public location: Location, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { }

  ngOnInit() {
    let key = 'fundTransDetails';
    this.order = JSON.parse(localStorage.getItem(key));
  }

  /*sell fund*/
  sellFund(order): void {
    var object = {};
    object['isin'] = this.order.portfolio.isin;
    object['scheme_code'] = this.order.portfolio.scheme_code;
    object['folio_no'] = this.order.portfolio.folio_no;

    localStorage.removeItem('reedm-success-flag');
    var addnProp = {};
    let key = "addnProp"
    addnProp['folio_no'] = this.order.portfolio.folio_no;
    addnProp['sip_frequency'] = this.order.portfolio.sip_frequency;
    localStorage.setItem(key, JSON.stringify(addnProp));

    this.loading = true;
    this.apiService.getRedeemAmount(object).subscribe(
      res => {
        let key = 'sellFundDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        this.router.navigateByUrl('/sell-fund');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }  
        // if (this.errorStatus == "403") {
        //   localStorage.clear();
        //   this.router.navigate(['/login']);
        // }
      }
    )

    this.apiService.getMinRedeemAmount(object).subscribe(
      res => {
        this.minRedeemAmt = res.data;
        localStorage.setItem("minRedeemAmt",JSON.stringify(this.minRedeemAmt))
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }  

        // if (this.errorStatus == "403") {
        //   localStorage.clear();
        //   this.router.navigate(['/login']);
        // }
      }
    )

  }
  /*sell fund*/

  /*invest more*/
  investMore(): void {
    let key = 'scheme_code';
    var object = {};
    this.loading = true;
    object['isin'] = this.order.portfolio.isin;
    object['scheme_code'] = this.order.portfolio.scheme_code;

    this.apiService.getFundDetails(object).subscribe(
      res => {
        this.loading = false;
        let key = 'scheme_details';
        localStorage.setItem(key, JSON.stringify(res.data));
        let isinkey = 'scheme_isin';
        localStorage.setItem(isinkey, JSON.stringify(this.order.portfolio.isin));
        this.router.navigate(['/fund-details']);
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);

        // if (this.errorStatus == "403") {
        //   localStorage.clear();
        //   this.router.navigate(['/login']);
        // }
      }
    )

  }
  /*invest more*/

}
