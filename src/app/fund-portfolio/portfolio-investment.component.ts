import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label, Color, BaseChartDirective } from 'ng2-charts';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

enum CheckBoxType { Inv_High_to_Low, Cur_High_to_Low, Pl_High_to_Low, Inv_Low_to_High, Cur_Low_to_High, Pl_Low_to_High, NONE };

@Component({
  selector: 'app-portfolio-investment',
  templateUrl: './portfolio-investment.component.html',
  styleUrls: ['./portfolio-investment.component.css']
})

export class PortfolioInvestmentComponent implements OnInit {
  viewMode = 'tab1';
  showFilterSec: boolean;
  loading: boolean;
  otherError: boolean;
  public isViewable: boolean;
  errorMessage: any;
  errorStatus: any;
  inv: any;
  cur: any;
  pl: any;
  item1: any;
  item2: any;
  portfolioDetails = [];
  portfolioAnalysis = [];
  newFundCompany = [];
  newFundpercentage = [];
  newFundDetails = [];
  order = [];
  noInvest = false;
  showFilter = false;

  constructor(private router: Router, public location: Location, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { 
    this.portFolioDetails();
    this.getPortfolio();
  }

  ngOnInit() {
    this.isViewable = true;
    this.loading = true;
  }

  portFolioDetails() {
    this.apiService.getPortfolioAnalysis().subscribe(
      res => {
        this.loading = false;
        this.portfolioAnalysis = res.data;
        // Loop to show analysis chart
        for (let i = 0; i <= this.portfolioAnalysis.length - 1; i++) {
          let fundCategory;
          fundCategory = this.portfolioAnalysis[i].scheme_category;
          this.newFundCompany.push(fundCategory);

          let fundPercentage;
          fundPercentage = this.portfolioAnalysis[i].percent;
          this.newFundpercentage.push(fundPercentage);

          let object = {};
          object['scheme_category'] = this.portfolioAnalysis[i].scheme_category;
          object['percent'] = this.portfolioAnalysis[i].percent;
          object['color'] = this.bgcolor[i];
          this.newFundDetails.push(object);
        }
        this.updatePLdoughnut();
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  getPortfolio() {
    this.apiService.getPortfolioDetails().subscribe(
      res => {
        this.loading = false;
        this.portfolioDetails = res.data;
        let inv = 0;
        let cur = 0;
        for (let i = 0, n = this.portfolioDetails.length; i < n; i++) {
          inv += this.portfolioDetails[i].invested_amount;
          cur += this.portfolioDetails[i].current_valuation;
        }
        this.inv = inv.toFixed(2);
        this.cur = (cur).toFixed(2);
        this.pl = ((cur - inv) / inv * 100).toFixed(2);
        if (this.pl == 'NaN') {
          this.pl = 0;
        }

        // Condition to show no-investment and filter section
        if (this.portfolioDetails.length != 0) {
          this.noInvest = false;
          this.showFilter = true;
        }
        else {
          this.noInvest = true;
          this.showFilter = false;
        }
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  // Filter section
  selectedUserTab = 1;
  tabs = [
    { name: 'Invested', key: 1, active: true },
    { name: 'Current', key: 2, active: false },
    { name: 'P&L', key: 3, active: false },
  ];

  tabChange(selectedTab) {
    this.selectedUserTab = selectedTab.key;
    for (let tab of this.tabs) {
      if (tab.key === selectedTab.key) {
        tab.active = true;
      }
      else {
        tab.active = false;
      }
    }
  }

  // Function to select only one checkbox at once 
  check_box_type = CheckBoxType;
  currentlyChecked: CheckBoxType;

  selectCheckBox(targetType: CheckBoxType) {
    // If the checkbox was already checked, clear the currentlyChecked variable
    if (this.currentlyChecked === targetType) {
      this.currentlyChecked = CheckBoxType.NONE;
      return;
    }
    this.currentlyChecked = targetType;
  }

  cancelBtn(event: Event) {
    this.showFilterSec = !this.showFilterSec;
    event.stopPropagation();
  }
  // Filter section

  showOrHideBankName() {
    this.showFilterSec = !this.showFilterSec;
  }

  // Function to view individual fund transaction
  viewPortfolioDetails(order): void {
    var object = {};
    object['isin'] = order.isin;
    object['folio_no'] = order.folio_no;
    this.loading = true;

    this.apiService.getFundTransactions(object).subscribe(
      res => {
        let key = 'fundTransDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        this.router.navigateByUrl('/fund-transaction');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  /** Simple method to toggle element visibility */
  public toggle(): void { this.isViewable = !this.isViewable; }

  // Doughnut Chart 
  public doughnutChartOptions: any = {
    responsive: true,
    legend: {
      display: false,
      labels: {
        display: false
      }
    }
  };

  public bgcolor = ['#4671bf', '#26d6fb', '#c3fa60', '#56c374', '#1b9bfe', '#26d6d6', '#c3fafa', '#56c3c3', '#1b9b9b', '#C0C0C0', '#808080', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#00FFFF', '#008080', '#0000FF', '#D6EAF8', '#73C6B6', '#EC7063', '#AED6F1', '#6E2C00', '#D7BDE2'];
  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: MultiDataSet = [
    [],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public chartColors: Array<any> = [
    { // all colors in order
      backgroundColor: this.bgcolor
    }
  ]

  public updatePLdoughnut() {
    this.doughnutChartLabels = this.newFundCompany;
    this.doughnutChartData = this.newFundpercentage;
  }
  // Doughnut Chart 

  // Function to sort data high to low and vice-versa
  sortResults(prop, asc) {
    this.portfolioDetails.sort(function (a, b) {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
      }
    });
  }

  applyFilter() {
    if (this.currentlyChecked === this.check_box_type.Inv_High_to_Low) {
      this.sortResults('invested_amount', false);
      this.cancelBtn(event);
    }
    if (this.currentlyChecked === this.check_box_type.Inv_Low_to_High) {
      this.sortResults('invested_amount', true);
      this.cancelBtn(event);
    }

    if (this.selectedUserTab == 2) {
      if (this.currentlyChecked === this.check_box_type.Cur_High_to_Low) {
        this.isViewable = true;
        !this.isViewable == false;
        this.sortResults('current_valuation', false);
        this.cancelBtn(event);
      }
      if (this.currentlyChecked === this.check_box_type.Cur_Low_to_High) {
        this.isViewable = true;
        !this.isViewable == false;
        this.sortResults('current_valuation', true);
        this.cancelBtn(event);
      }
    }

    if (this.selectedUserTab == 3) {
      if (this.currentlyChecked === this.check_box_type.Pl_High_to_Low) {
        !this.isViewable == true;
        this.isViewable = false;
        this.sortResults('pl', false);
        this.cancelBtn(event);
      }
      if (this.currentlyChecked === this.check_box_type.Pl_Low_to_High) {
        !this.isViewable == true;
        this.isViewable = false;
        this.sortResults('pl', true);
        this.cancelBtn(event);
      }
    }
  }

}
